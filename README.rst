Ansible Vault and GPG
=====================

.. _`Ansible`: 
.. _`Ansible Vault`:

`Ansible Vault`_ can be used to encrypt Ansible_ configuration files (e.g.
those in ``host_vars`` and ``group_vars``). It uses symmetric encryption with
a preset key. If you work in a team sharing the key may become a complicated 
task. There are more solutions available. GnuPG is used in this project to 
address sharing the key.

Before you continue reading check following conditions.

  * You have a project in which you want to start using `Ansible Vault`_.
  * You have basic knowledge of Ansible_ and `Ansible Vault`_.
  * You want to share project with someone else and that someone should
    be able to decrypt encrypted configuration.
  * You and your colleagues have GPG keys and exchanged them (manually or 
    via a key server).

How it works
------------

The symmetric key used by `Ansible Vault`_ is stored within a project (e.g. 
in a VSC repository). It is encrypted using GPG. The key is treated as an 
*encrypted message* from GPG point of view. It has its *recipient(s)* who
can decrypt it.

If `Ansible Vault`_ needs the key it is configured execute a script which
decrypts the *encrypted message* and returns the key. The key is used by
`Ansible Vault`_ to decrypt configuration.

``gpg-agent`` is the last component used. It is optional but it eases use 
of GPG a lot. It opens keys and keeps them open for a while so you do not 
have to type password each time you use it. Note that GnuPG 2.1.0 and higher 
requires use of a ``gpg-agent`` thus the ``gpg-agent`` starts on demand. 
Older versions required a bit of a scripting to start the ``gpg-agent`` 
and make sure that just one is running (look at `gpg-agent.sh <gpg-agent.sh>`_
in this project).

Handbook
--------

  #. Create the symetric key. It should be long random string. It should not
     contain unsafe characters (it may be used within shell or CI). ::

       openssl rand 512 | base64 | head -c 64 | gpg --encrypt --output vault.gpg \
         --recipient foo@example.org --recipient bar@example.org ...

     Please make sure you have imported public keys of respective recipients.

  #. Create a script ``vault.sh`` that will be invoked each time the key is 
     required. Do not forget to make it executable. Decryption of the *encrypted 
     message* is pretty straightforward. ::

       #!/bin/bash
       gpg --batch --use-agent --decrypt vault.gpg

  #. Configure `Ansible Vault`_ password to be a script. Proper way is to add
     it to ``ansible.cfg`` in your project. ::

       [defaults]
       # vault password
       vault_password_file=vault.sh

  #. Add and commit changes made in the project (if you use VCS).
  #. Use `Ansible Vault`_ to store sensitive parts of your configuration in a 
     secure way.
  #. Share the project.

Notes on CI
-----------

If you use CI, e.g. GitLab CI, using GPG is not an option. The key is provided 
to a runner as a variable. It requires slight modification of ``vault.sh`` which 
must prefer the variable over GPG if set. ::

  #!/bin/bash
  [ -n "$ANSIBLE_VAULT_PASSPHRASE" ] && {
    echo "$ANSIBLE_VAULT_PASSPHRASE"
    exit 0
  }
  gpg --batch --use-agent --decrypt vault.gpg

Resources
---------

`vault.sh <vault.sh>`_ and `ansible.cfg <ansible.cfg>`_ are available in this 
project. You can use them in any project you like. This project is licensed 
under MIT license.

A lot of tutorials and guides are available on the Internet. A few of them are
listed bellow just for a convinience:

  * `Secrets with Ansible: Ansible Vault and GPG <https://benincosa.com/?p=3235>`_
  * `Managing Secrets with Ansible Vault – The Missing Guide (Part 1 of 2) <https://dantehranian.wordpress.com/2015/07/24/managing-secrets-with-ansible-vault-the-missing-guide-part-1-of-2/>`_
  * `Managing Secrets with Ansible Vault – The Missing Guide (Part 2 of 2) <https://dantehranian.wordpress.com/2015/07/24/managing-secrets-with-ansible-vault-the-missing-guide-part-2-of-2/>`_

.. vim: spelllang=en spell:
