#!/bin/bash

GPG_AGENT=$(which gpg-agent)
if [ ! -x "$GPG_AGENT" ]; then
    echo "gpg-agent binary not found" 1>&2
    return 1
fi    

DIR="$HOME/.gnupg"
LOGFILE="$DIR/gpg-agent.log"
ENVFILE="$DIR/gpg-agent.env"

PID=$(ps -eo comm,user,pid | grep "^gpg-agent[[:space:]]\+$USER" | grep -o '[0-9]\+$')

if [ -n "$PID" -a -f "$ENVFILE" ] && grep -qs ":$PID:" "$ENVFILE"; then
    :
else    

    if [ -n "$PID" ]; then
        kill -9 "$PID" 1>/dev/null 2>&1
    fi

    "$GPG_AGENT" \
        --daemon \
        --log-file="$LOGFILE" \
        --write-env-file "$ENVFILE" 1>/dev/null

    if [ $? -ne 0 ]; then
        echo "gpg-agent failed to start" 1>&2
        return 2
    fi
fi  

if [ -f "$ENVFILE" ]; then
    GPG_TTY=$(tty)
    export GPG_TTY
    source "$ENVFILE"
    export GPG_AGENT_INFO
fi
