#!/bin/bash

##
# Log message to STDERR. If severity is error it will terminate
# execution of this function.
# $1 message
# $2 severity, defaults to info
# $3 return code in case of error, defaults to 1
function log {
	local message="$1"
	local severity="${2:-info}"
	local code="${3:-1}"
	echo "[$severity] $message" >&2
	if [ "$severity" = 'error' ]; then
		exit "$code"
	fi
}

##
# Generate a random key using pwgen or openssl
# $1 length of the key, defaults to 128
function genkey {
		local length="${1:-128}"

		if which pwgen 1>/dev/null 2>&1; then
			pwgen -n -1 "$length"
		elif which openssl 1>/dev/null 2>&1 && which base64 1>/dev/null 2>&1; then
			openssl rand "$((4 * length))" | base64 | head -c "$length"
		else
			log "pwgen or openssl must be installed in order to generate a key." error
		fi
}

# If the key is present in a variable it is used.
[ -n "$ANSIBLE_VAULT_PASSPHRASE" ] && {
	log 'Using variable ANSIBLE_VAULT_PASSPHRASE'
    echo "$ANSIBLE_VAULT_PASSPHRASE"
    exit 0
}

# Retrieve, generate or rencrypt the key using GPG
keyfile="vault.gpg"
declare -a recipients
#recipients=("recipient1@example.org" "recipient2@example.org")

keyfile="/tmp/vault.gpg"
recipients=("vaclam1@fit.cvut.cz")

case "$1" in
	# generate a new key
	"genkey")
		[ -f "$keyfile" ] && log "$keyfile exists already. No action taken." error
		# merge recipients and command line arguments
		shift 1
		recipients=("${recipients[@]}" "$@")
		# generate a key and store it encrypted by recipients' keys
		genkey | gpg2 --encrypt --batch --use-agent --armor "${recipients[@]/#/--recipient=}" --output "$keyfile" \
			&& log "A new key has been generated. Recipients are: ${recipients[*]}" \
			|| log "Failed to generate a new key." error
		;;

	# reencrypt the key by recipients' keys
	"recipients")
		[ -f "$keyfile" ] || log "$keyfile does not exist." error
		# merge recipients and command line arguments
		shift 1
		recipients=("${recipients[@]}" "$@")
		# reencrypt
		gpg2 --decrypt --use-agent --batch --no-verbose --quiet "$keyfile" |
			gpg2 --encrypt --batch --yes --use-agent --armor "${recipients[@]/#/--recipient=}" --output "$keyfile" \
				&& log "The key has been reencrypted. Recipients are: ${recipients[*]}" \
				|| log "Failed to reencrypt the key." error
		;;
				
	# retrive the key
	*)
		[ -f "$keyfile" ] || log "$keyfile does not exist." error
		gpg2 --decrypt --use-agent --batch --no-verbose --quiet "$keyfile" | head -n1 \
            || log "Failed to decrypt the key" error
		;;
esac

